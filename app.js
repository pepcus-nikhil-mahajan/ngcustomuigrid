var app = angular.module('app', [
  'ui.grid',
  'ui.grid.selection',
  'ui.grid.exporter',
  'ui.grid.pagination',
  'ui.grid.infiniteScroll',
  'ui.grid.moveColumns'
]);

app.controller('MainCtrl',['$http','$scope', 'uiGridConstants', function ($http,$scope, uiGridConstants) {
  var thisVar = this;

  var data =[];

  // $scope.addData = function() {
  //   var n = thisVar.gridOptions.data.length + 1;
  //   thisVar.gridOptions.data.push({
  //     "name": "New " + n,
  //     "age": n,
  //     "employed": true,
  //     "gender": "male"
  //   });
  // };
  //
  // $scope.removeFirstRow = function() {
  //   thisVar.gridOptions.data.splice(0,1);
  // };

  thisVar.gridOptions = {
    // Explai
    infiniteScrollRowsFromEnd: 40,
    //
    infiniteScrollUp: true,
    //
    infiniteScrollDown: true,
    //
    showGridFooter: true,
    //
    showColumnFooter: true,
    //
    paginationPageSizes: [25, 50, 75, 100],
    paginationPageSize: 50,
    enableFiltering: true,
    enableGridMenu: true,
    enableSelectAll: true,
    // Make it configurable
    exporterCsvFilename: 'myFile.csv',
    exporterPdfDefaultStyle: {fontSize: 9},
    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
    exporterPdfFooter: function ( currentPage, pageCount ) {
      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
    },
    exporterPdfCustomFormatter: function ( docDefinition ) {
      docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
      docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
      return docDefinition;
    },
    exporterPdfOrientation: 'portrait',
    exporterPdfPageSize: 'LETTER',
    exporterPdfMaxGridWidth: 500,
    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
    exporterExcelFilename: 'myFile.xlsx',
    exporterExcelSheetName: 'Sheet1',
    onRegisterApi: function(gridApi){
      thisVar.gridApi = gridApi;
    },
    columnDefs: [
      {
        field: 'name',
        cellTooltip: function (row, col) {
          return ' name: ' + row.entity.name;
        },
        width: '150'
      },
      {
        field: 'company',
        cellTooltip: function (row, col) {
          return ' Company: ' + row.entity.company;
        },
        aggregationType: uiGridConstants.aggregationTypes.sum, width: '150'
      },
      {
        field: 'age', cellTooltip: function (row, col) {
          return ' age: ' + row.entity.age;
        }, aggregationType: uiGridConstants.aggregationTypes.avg, aggregationHideLabel: false, width: '150'
      },
      {
        name: 'ageMin',
        field: 'age',
        aggregationType: uiGridConstants.aggregationTypes.min,
        width: '150',
        displayName: 'Age for min'
      },
      {
        name: 'ageMax',
        field: 'age',
        aggregationType: uiGridConstants.aggregationTypes.max,
        width: '150',
        displayName: 'Age for max'
      },
      {
        name: 'customCellTemplate',
        field: 'age',
        width: '150',
        footerCellTemplate: '<div class="ui-grid-cell-contents" style="background-color: Red;color: White">custom template</div>'
      },
      {
        name: 'registered',
        field: 'registered',
        width: '150',
        cellFilter: 'date',
        footerCellFilter: 'date',
        aggregationType: uiGridConstants.aggregationTypes.max
      }
    ]
  };
  $http.get('data1.json')
    .then(function(response) {
      var data = response.data;
      thisVar.gridOptions.data = data;
    });

}]);

